## TODO List (Laravel + VueJS)

A project which allows to work with TODO lists driven by Laravel and VueJS.

## Specification

- **[Laravel 8.0](https://laravel.com/docs/8.x/releases)**
- **[VueJS 2.5.17](https://vuejs.org/)**

## Installation

Follow the next steps to install the project:

1. Clone repository
2. Change PHP version in the .htacess file according to your installed PHP version.
3. Go into the project folder and run `composer update`. **[Composer](https://getcomposer.org/)**
4. Check that `/storage` folder has correct right, if not, run `sudo chmod -R 777 storage`
5. Run `cp .env.example .env` to create a configuration file for your project
6. Create a database and fill the database credentials (name, user, pass, etc.) of the new database to the `DB_*` parameters in to your .env file
7. Run `php artisan key:generate` to generate application encryption key
8. Fill `APP_*` parameters in the .env file according to your environment (`APP_URL`, `APP_ENV`, etc.)
9. Run `php artisan migrate` to create required tables and fill them with required data from migrations. If something went wront, make sure that you fill the `DB_*` parameters correctly.

## Author

Mikhail Denisov
